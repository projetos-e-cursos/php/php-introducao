# PHP Introdução a linguagem

## Curso de php iniciante Alura.

### Conceitos abordados:
- O que é PHP;
- Onde PHP é utilizado;
- Variáveis;
- Tipos de dados;
- Operações;
- strings;
- Caracteres de escape;
- PHP_EOL;
- Estruturas condicionais;
- Operadores lógicos;
- Operadores relacionais;
- Estruturas de repetição;
- continue e break;
