<?php

$idade = 22;
// $nome = "Erick";
$numeroDePessoas = 2;

echo "Você só pode entrar se tiver a partir de 18 anos." . PHP_EOL;

if ($idade >= 18)
    echo "Você tem $idade anos. Pode entrar sozinho.";
else if ($idade >= 16 and $numeroDePessoas > 1)
    echo "Você tem $idade anos, está acompanhado(a), então pode entrar";
else
    echo "Você tem $idade anos.\n Não pode entrar!!!";


echo PHP_EOL;
echo "Adeus!";
